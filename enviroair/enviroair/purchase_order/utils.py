from django.utils.crypto import get_random_string


def generate_new_purchase_order_number():
    return get_random_string(5, '0123456789')


def get_unique_purchase_order_number(purchase_order_model, new_number=None):
    number = new_number
    if new_number is None:
        number = generate_new_purchase_order_number()

    if purchase_order_model.objects.filter(number=number).exists():
        get_unique_purchase_order_number(purchase_order_model)

    return number
