from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.timezone import now


# sort order (manual configure and then alphabetical)
class SortableModel(models.Model):
    sort_order = models.PositiveIntegerField(null=True, blank=True, )

    class Meta:
        abstract = True
        ordering = ['sort_order']


class Job(SortableModel):
    created = models.DateTimeField(default=now)

    name = models.CharField(max_length=250)

    number = models.SlugField(max_length=255, unique=True)

    class Meta(SortableModel.Meta):
        # sort order (manual configure and then alphabetical)
        ordering = SortableModel.Meta.ordering + ['name']

    # an other selections field and
    # free text field for other jobs

    # file import for first start (from CSV)

    def __str__(self):
        return f"{self.name}"


class Supplier(SortableModel):
    name = models.CharField(max_length=250)

    class Meta(SortableModel.Meta):
        # sort order (manual configure and then alphabetical)
        ordering = SortableModel.Meta.ordering + ['name']

    # an other selections field and
    # free text field for other suppliers

    # file import for first start (from CSV)

    """email  = models.EmailField(blank=True, default="")"""

    def __str__(self):
        return f"{self.name}"


class PurchaseOrder(models.Model):
    created = models.DateTimeField(default=now)

    number = models.SlugField(max_length=255, unique=True)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="purchase_orders",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    job = models.ForeignKey(
        Job,
        related_name="purchase_orders",
        null=True,
        on_delete=models.SET_NULL,
    )

    supplier = models.ForeignKey(
        Supplier,
        related_name="purchase_orders",
        null=True,
        on_delete=models.SET_NULL,
    )

    description = models.TextField(blank=True)

    class Meta:
        ordering = ['-created', '-number']

    def __str__(self):
        return f"{self.number} - {self.supplier} - {self.created.strftime('%d %b %Y')}"

    def get_absolute_url(self):
        return reverse('purchase_order:purchase-order-detail', kwargs={'pk': self.pk})
