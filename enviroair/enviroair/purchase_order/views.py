from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .models import Job, Supplier, PurchaseOrder
from .utils import get_unique_purchase_order_number


class PurchaseOrderListView(ListView):
    model = PurchaseOrder
    paginate_by = 10


class PurchaseOrderDetailView(DetailView):
    model = PurchaseOrder


class PurchaseOrderCreateView(PermissionRequiredMixin, CreateView):
    model = PurchaseOrder
    fields = ['supplier', 'job', 'description']
    success_message = 'Purchase Order was created successfully. Please use: %(number)'
    permission_required = 'purchase_order.can_create'

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.number = get_unique_purchase_order_number(PurchaseOrder)
        return super().form_valid(form)


class PurchaseOrderUpdateView(PermissionRequiredMixin, UpdateView):
    model = PurchaseOrder
    fields = ['number', 'supplier', 'job', 'description']
    success_message = 'Purchase Order %(number)s was updated successfully'
    permission_required = 'purchase_order.can_update'


class PurchaseOrderDeleteView(PermissionRequiredMixin, DeleteView):
    model = PurchaseOrder
    success_url = reverse_lazy('purchase_order:purchase-order-list')
    permission_required = 'purchase_order.can_delete'
