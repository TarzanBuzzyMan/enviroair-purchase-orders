from django.urls import path

from . import views

urlpatterns = [
    path('', views.PurchaseOrderListView.as_view(), name="purchase-order-list"),
    path('<int:pk>/', views.PurchaseOrderDetailView.as_view(), name="purchase-order-detail"),
    path('create/', views.PurchaseOrderCreateView.as_view(), name="purchase-order-create"),
    path('update/<int:pk>/', views.PurchaseOrderUpdateView.as_view(), name="purchase-order-update"),
    path('delete/<int:pk>/', views.PurchaseOrderDeleteView.as_view(), name="purchase-order-delete"),
]
