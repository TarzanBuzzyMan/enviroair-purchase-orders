from django.contrib import admin

from .models import Job, Supplier, PurchaseOrder

admin.site.register(Job)
admin.site.register(Supplier)


class PurchaseOrderAdmin(admin.ModelAdmin):
    list_display = ('number', 'job', 'supplier', 'user')
    list_display_links = ('number', 'job', 'supplier', 'user')
    list_filter = ('job', 'supplier', 'user')
    search_fields = ['number']


admin.site.register(PurchaseOrder, PurchaseOrderAdmin)
